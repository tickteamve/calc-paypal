Vue.config.devtools = true

new Vue({
	el: '#app',
	created() {
		this.getApproximateRate()
	},
	data: {
		mode: 'paypal',// Modo de calculadora
		sent: 10, // Cantidad que se envia
		quantity: null, // Cantidad necesaria 
		commission: null, // Comisión
		rate: null, // Tasa
		// Dolar today data
		time: null,
		paypal: null,
		dolartoday: null,
		btc: null
	},
	methods: {
		getApproximateRate: function () {
			axios.get('https://s3.amazonaws.com/dolartoday/data.json').then(res => {
				let value = res.data.USD
				let dolartoday = value.dolartoday.toString().substring(0, 3)
				let btc = (parseInt(value.bitcoin_ref.toString().substring(0, 3)) + parseInt(value.localbitcoin_ref.toString().substring(0, 3))) / 2				
				this.dolartoday = dolartoday * 1000
				this.paypal = parseInt((dolartoday - 25) * 1000)
				this.btc = btc * 1000

				this.rate = this.paypal
				this.time = res.data._timestamp.fecha
			})
		},
		copy: function () {
			let aux = document.createElement('input')
			let sent = (this.mode == 'paypal') ? parseFloat(this.receive.substr(0, this.receive.length)) : this.sent
			let rate = (this.mode == 'paypal') ? this.paypal : this.dolartoday
			let text = `${sent} $ x ${this.formatAmount(rate)} = ${this.formatAmount(this.totalbs)}`
			aux.setAttribute('value', text)
			document.body.appendChild(aux)
			aux.select()
			document.execCommand('copy')
			document.body.removeChild(aux)
		},
		formatAmount: function (amount) {
			totalbs = parseFloat(amount)
			amount += ''; 
			amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
		    if (isNaN(amount) || amount === 0) 
		    	return parseFloat(0)
		    amount = '' + amount
		    var amount_parts = amount.split('.'),
		    regexp = /(\d+)(\d{3})/;
		    while (regexp.test(amount_parts[0]))
		    	amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');
		    return `${amount_parts.join(',')} Bs`;
		}
	},
	computed: {
		necessary: function () {
			if (this.quantity > 0) {
				let necessary = ((parseFloat(this.quantity) + 0.30) / (94.6) * 100).toFixed(2)
				return necessary
			}
		},
		receive: function () {
			if (this.sent > 0) {
				let commission = (`${(parseFloat(this.sent * 0.054) + 0.3).toFixed(2)}`) || 0
				let receive = (this.sent - commission).toFixed(2) || 0
				this.commission = commission
				return receive
			} else {
				return ''
			}
		},
		totalbs: function () {
			if (this.mode === 'paypal') {
				this.rate = this.paypal
			} else if (this.mode === 'efectivo') {
				this.rate = this.dolartoday
			} else {
				this.rate = this.btc
			}

			if (this.rate > 0) {
				if (this.mode === 'paypal') {
					let receive = parseFloat(this.receive)
					console.log(receive)
					return `${(receive * this.rate).toFixed(0)}`
				} else if (this.mode === 'efectivo') {
					return `${this.sent * this.rate}`
				} else {
					return `${this.sent * this.rate}`
				}
			}
		}
	},
	filters: {
		format: function (totalbs, simbol) {
			totalbs = parseFloat(totalbs)
			totalbs += ''; 
			totalbs = parseFloat(totalbs.replace(/[^0-9\.]/g, ''));
		    if (isNaN(totalbs) || totalbs === 0) 
		    	return parseFloat(0)
		    totalbs = '' + totalbs
		    var amount_parts = totalbs.split('.'),
		    regexp = /(\d+)(\d{3})/;
		    while (regexp.test(amount_parts[0]))
		    	amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');
		    return `${amount_parts.join('.')} ${simbol}`;
		}
	}
})
